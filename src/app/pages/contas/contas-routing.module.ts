import { ListaPage } from './lista/lista.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RelatorioPage } from './relatorio/relatorio.page';
import { CadastroPage } from './cadastro/cadastro.page';
import { ReceberPage } from './receber/receber.page';
import { PagarPage } from './pagar/pagar.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'', children: [
      {path: 'pagar', component: ListaPage},
      {path: 'receber', component: ListaPage},
      {path: 'cadastro', component: CadastroPage},
      {path: 'relatorio', component: RelatorioPage}
    ]
  },
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PagarPage,
    ReceberPage,
    CadastroPage,
    RelatorioPage,
    ListaPage
  ]
})
export class ContasRoutingModule { }
