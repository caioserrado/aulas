import { CalculadoraService } from './../../services/calculadora.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  titulo = 'Calculadora';
  form: FormGroup;
  quociente: string | number;

  constructor(
      private builder: FormBuilder,
      private calculadora: CalculadoraService
  ) { }

  ngOnInit() {
    this.form = this.builder.group({
       dividendo: ['', [Validators.required]],
       divisor: ['', [Validators.required]] 
    });
  }

  dividir(){
    const data = this.form.value;
    const divisor = data.divisor;
    const dividendo = data.dividendo;

    this.quociente = this.calculadora.divide(dividendo, divisor);
  }

}
